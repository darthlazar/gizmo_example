
## Here I provide a quick overview of simulating a low-res Milky Way-mass halo using the zoom-in technique.


This was done on the local UCI cluster using the [public version](https://bitbucket.org/phopkins/gizmo-public/src/master/) of `GIZMO`. I've included files that aren applicable to the numerics done at each timestep and more.
I ran this back during my first quarter of graduate school. I believe this took about 20 hours to run to completion based on the node configuration I have specified.

---

### 1) Initial Conditions:

This is found in the `IC` folder. A `CAMB` generated power spectrum file `hyades_transfer_out_z125.dat` is compiled using `MUSIC`. 

Opening the example config file `GVD_Z10_1TBD_Zeus_z125_5rvir.conf`, this specifies the characteristics of simulation box.
Most important values are the box length, `boxlength`, starting redshift, `zstart`, and the level of grid refinements:
The least refined grid, `levelmin`, the level at which the transfer function is computed (reproducable with the specified seed), `levelmin_TF`, and the highest resolved grid of which the halo is formed, `levelmax`. 
One must also specify the seed at which the transfer function is computed. Once the `.conf` file has been specified, the `MUSIC` binary is used to 
compile it to generate the white noise and outputs the `GIZMO` readable output IC file, i.e. `GVD_Z10_1TBD_Zeus_z125_5rvir_ICs.gadget`. 
Note that since this is a zoom simulation, its best that the particles in each resolution level must be partitioned into different particle types for more straightforward analysis. 
These files are given in the `split` sub-directory, where `GVD_Z10_1TBD_Zeus_z125_5rvir_ICs.gadget` is hit with `split_gb.py` to make the `GVD_Z10_1TBD_Zeus_z125_5rvir_ICs_split.gadget` binary used by `GIZMO`.

---

### 2) GIZMO set-up

We use the publicly available version of `GIZMO`.  
In order to run this properly, make sure your system has `FFTW2`, `MPI`, `GSL`, and `HDF5`. 
For further curoisity, look at the specifications of the `greenplanet` system type inside the `Makefile`. To download the dependencies see the following [link](http://www.tapir.caltech.edu/~phopkins/Site/GIZMO_files/gizmo_documentation.html#tutorial).

`Template_Config.sh` has all the parameters you can dream of to run your numerical simulations. For collisionless dark matter, you ~10 parameters. 
For the parameters considered for this low-res sim, take a look at the `Config.sh`. These specified more precisly in the `GIZMO` documentation. 
Once these configurations have been set, run the `Makefile`. 
Please note that on wherever you are running this, make sure all of the above dependencies are installed and the paths are pointed to correctly to the binaries of the machine. 
You can go ahead and make your own `SYSTYPE` with the appropriate paths and flags. You will then unccoment the machine that you have defined in `Makefile.systype`.

Even if the `GIZMO` binary is compiled, run it by `./GIZMO` and make sure no errors are outputted!

---

### 3) GIZMO parameter file

Once the `GIZMO` binary has been compiled, we set up the parameter file, which is the last step to start running the simulation. 
Everything you need will be in the `param` folder.
The `a_out_list.txt` is used to save the simulation into snapshots at the specificied scale factor. 
The `gizmo.param` specifies the file input and output paths as well as the numerical parameters, I'd start being familiar with the values found inside. 
All of those specifics are found in the original `GADGET-2` documentation found online.

The `gizmo.param` file will have to be tuned to the system you are running `GIZMO` on. 
Taking a look at the parameters, this is what you should keep in mind:

1. `InitCondFile`: The path that points to where the initial conditions location
2. `OutputDir`: where the simulation files are outputted in
3. `MaxMemSize`: The MPI process memory in megabytes. The 8 core node I am using has a memory of 24 Gigabytes. So it should be set to that value in Megabytes. If running in parallel on a single core, divide the number of prcoesses with the total memory of the node.
4. `PartAllocFactor`: Memory load allowed for CPU adjustment. Should be played with.
5. `TreeAllocFactor`: Memory load allowed for tree adjustment. Should be played with.

Job submissions are done through `SLURM` -- the bash file I used to run the simulation on a single node is `run_gizmo.slurm` using `MPI`. I will update when need be.

---

### 4) output

When running, the snapshot files will be outputted into the `output/hdf5` directory, as specified in the `gizmo.param` file.
I've included the redshift `z=0` snapshot (`snapshot_152.hdf5`) as an example which formed a `1e+12` solar mass dark matter halo/
Additional statistics of each timestep will be outputted in `output/hdf5`. 
What will be of interest is `cpu.txt`, which contatins the amount of CPU's has been allocated by the gravity solver.