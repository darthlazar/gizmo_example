#!/bin/bash

#HAVE_HDF5
BOX_PERIODIC               
#PMGRID=512 
PMGRID=128                 
#OPENMP=2                   
#MULTIPLEDOMAINS=16         
#MULTIPLEDOMAINS=4        
#OUTPUT_POSITIONS_IN_DOUBLE 
#DOUBLEPRECISION_FFTW
#PM_PLACEHIGHRESREGION=2
